USE [master]
GO
DROP DATABASE [ecommerce]
GO
CREATE DATABASE [ecommerce]
GO
USE [ecommerce]
GO
CREATE TABLE [product](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[name] [nvarchar](50) NOT NULL UNIQUE,
	[price] float check(price >= 0 and price <= 10000),
	[description] [ntext] 
)
GO
insert into product([name],[price],[description])
values
	('IPad',700,N'Máy tính bảng'),
	('IPod',100,N'Máy nghe nhạc'),
	('IPhone 6s',800,N'Điện thoại cao cấp'),
	('IPhone 5',500,N'Điện thoại trung cấp'),
	('IPhone 4',200,N'Điện thoại tầm trung')


--select all
select [id],[name],[price],[description] 
from product

--select by name
select [id],[name],[price],[description] 
from product
where [name] like '%phone%'

CREATE TABLE customer
(
	id INT IDENTITY(1,1) PRIMARY KEY,
	name VARCHAR(250),
	phone VARCHAR(50),
	address VARCHAR(50),
	total float
)
